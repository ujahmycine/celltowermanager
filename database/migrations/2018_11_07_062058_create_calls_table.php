<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caller_id')->unsigned();
            $table->integer('receiver_id')->unisgned();
            $table->boolean('connected');
            $table->integer('airtime');
            $table->integer('charge');
            $table->timestamps();
        });

        DB::table('calls')->insert([ //,
               'caller_id' => 1,
                'receiver_id' => 1,
                'airtime' => 50,
                'connected' => 1,
                'charge'=> 5,
                
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
                

            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
