<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->unique();
            $table->integer('base_station_id')->unsigned();
            $table->integer('balance')->default(100);
            $table->integer('airtime')->default(1000);
        
            $table->integer('last_recharge');
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });

        DB::table('sims')->insert([ //,
               'number' =>"08064012879",
                'balance' => 500,
                'airtime' => 5000,
                'last_recharge' => 200,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'base_station_id'=>1,
                'user_id' => 1

            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sims');
    }
}
