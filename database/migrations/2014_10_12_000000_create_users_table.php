<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned()->default(2);
            $table->string('first_name');
            $table->string('other_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->text('next_of_kin');
            $table->string('nok_phone');
            $table->string('alt_phone')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->default('$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'); // secret
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([ //,
               'first_name' =>"Johson",
                'last_name' => "Dalton",
                'other_name' => "Millet",
                'email' => "user@mtn.com",
                'nok_phone' => "08012345678",
                'alt_phone' => "08012345678",
                'next_of_kin' => 'jaymycine', 
                'remember_token' => str_random(10),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'role_id'=>1
                

            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
