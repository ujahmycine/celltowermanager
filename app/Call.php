<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sim;

class Call extends Model
{
    public function callNo()
    {
    	return $this->belongsTo(Sim::class, 'caller_id');
    }
    public function receiverNo()
    {
    	return $this->belongsTo(Sim::class, 'receiver_id');
    }
}
