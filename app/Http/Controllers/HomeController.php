<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BaseStation;
use Auth;
use App\Sim;
use App\Call;
use App\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('home')->with('users', $users);
    }
    public function log(Request $request)
    {
        $sim_id = Sim::where('user_id', $request['user_id'])->first()->id;
        $calls = Call::where('receiver_id', $sim_id)
                    ->orWhere('caller_id', $sim_id)->get();
        return view('home')->with('calls', $calls);
    }

    public function stationReport(Request $request)
    {
        
        $stations = BaseStation::all();
        return view ('home')->with('stations', $stations);

    }

    public function singleStationReport(Request $request)
    {
        
        $sims = Sim::where('base_station_id', $request['id'])->get();
        $base = BaseStation::find($request['id']);
        return view('home')->with('sims', $sims)->with('base', $base);
    }
}
