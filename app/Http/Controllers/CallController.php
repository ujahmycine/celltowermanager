<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sim;
use App\Call;
use App\BaseStation;
use Auth;

class CallController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function initiate()
    {
    	$sim_id = Sim::where('user_id', Auth::user()->id)->first()->id;
        $calls = Call::where('receiver_id', $sim_id)
                    ->orWhere('caller_id', $sim_id)->get();
    	$numbers = Sim::all()->where('user_id', '!=', Auth::user()->id);

    	$myNumber = Sim::where('user_id', Auth::user()->id )->get();


    	return view ('calls.call')->with('numbers', $numbers)->with('myNumber', $myNumber)->with('calls', $calls);
    }
    public function isBaseStationFree($caller, $receiver)
    {
    	$base_caller = BaseStation::find($caller->base_station_id);

    	if ($base_caller->connected_lines >= $base_caller->capacity)
    	{
    		return false;
    		$base_caller->connected_lines = $base_caller->connected_lines + 1;
    		$base_caller->save();
    	}
    	else
    	{
    		$base_rec = BaseStation::find($receiver->base_station_id);
    		if (!$base_rec)
    		{
    			$base_caller->connected_lines = $base_caller->connected_lines + 1;
    			$base_caller->save();
    			return true;
    		}
    		else
    		{
    			if ($base_rec->connected_lines >= $base_rec->capacity)
    			{
    				return false;
    			}
    			else
    			{
    				$base_caller->connected_lines = $base_caller->connected_lines + 1;
    				$base_caller->save();
    				$base_rec->connected_lines = $base_rec->connected_lines + 1;
    				$base_rec->save();
    				return true;
    			}
    		}
    	}
    }

    public function callLog($caller, $receiver)
    {
    	$log = new Call();
    	$log->caller_id = $caller->id;
    	$log->receiver_id = $receiver->id;
    	$log->charge = 0;
    	$log->airtime = 0;
    	$log->connected = 0;
    	$log->save();
    }
    public function updateLog($caller, $receiver, $airtime)
    {
    	$log = Call::where('caller_id',$caller->id)
    			->where('receiver_id',$receiver->id)
    			->latest()->first();
    	$log->airtime = $airtime;
    	$log->charge = ($airtime/10);
    	$this->updateSim($caller, $airtime);
    	$log->connected = 1;
    	$log->save();
    }

    public function updateSim($caller, $airtime)
    {
    	$sim = Sim::find($caller->id);
    	$sim->airtime = $sim->airtime-$airtime;
    	$sim->balance =$sim->balance -($airtime/10);
    	$sim->save();
    }
    public function calling (Request $request)
    {
    	$caller = Sim::find($request['caller_id']);
    	$receiver = Sim::find($request['receiver_id']);
    	$this->callLog($caller, $receiver);
    	$baseStationFree = $this->isBaseStationFree($caller, $receiver);
    	$response = ($baseStationFree) ? 'true' : 'false';

    	if($caller->airtime <= 0)
    	{
    		return $response ;
    	}

    	if($baseStationFree)
    	{
    		return $response;
    	}
    	else
    	{
    		return $response;
    	}

    }
    public function releaseBaseStation($caller, $receiver)
    {
    	$base_caller = BaseStation::find($caller->base_station_id);
    	$base_caller->connected_lines = $base_caller->connected_lines - 1;
    	$base_caller->save();
    	$base_rec = BaseStation::find($receiver->base_station_id);
    	$base_rec->connected_lines = $base_rec->connected_lines + 1;
    	$base_rec->save();

    	
    }

    public function endCall(Request $request)
    {
    	$data = "call ended";
    	return $data;
    	$caller = Sim::find($request['caller_id']);
    	$receiver = Sim::find($request['receiver_id']);
    	$airtime = ($request['airtime']);
    	$this->UpdateCallLog($caller, $receiver, $airtime);
    	$baseStationFree = $this->releaseBaseStation($caller, $receiver);
    	$data = "call ended";
    	return $data;
    }
}
