<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Location;
use App\Country;

class State extends Model
{
    public function locations()
    {
    	return $this->hasMany(Location::class);
    }

    public function country()
    {
    	return $this->belongsTo(Country::class);
    }


}
