<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sim extends Model
{
    public function baseStation()
    {
    	return $this->belongsTo(BaseStation::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
