@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="other_name" class="col-md-4 col-form-label text-md-right">{{ __('Other Names') }}</label>

                            <div class="col-md-6">
                                <input id="other_name" type="text" class="form-control{{ $errors->has('other_name') ? ' is-invalid' : '' }}" name="other_name" value="{{ old('other_name') }}" required autofocus>

                                @if ($errors->has('other_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('other_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="last_name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="nok" class="col-md-4 col-form-label text-md-right">{{ __('Next of Kin') }}</label>

                            <div class="col-md-6">
                                <input id="nok" type="nok" class="form-control{{ $errors->has('nok') ? ' is-invalid' : '' }}" name="nok" value="{{ old('nok') }}" required>

                                @if ($errors->has('nok'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nok') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="nok_phone" class="col-md-4 col-form-label text-md-right">{{ __('Next of Kin Phone') }}</label>

                            <div class="col-md-6">
                                <input id="nok_phone" type="nok_phone" class="form-control{{ $errors->has('nok_phone') ? ' is-invalid' : '' }}" name="nok_phone" value="{{ old('nok_phone') }}" required>

                                @if ($errors->has('nok_phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nok_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="alt_phone" class="col-md-4 col-form-label text-md-right">{{ __('alternative_phone') }}</label>

                            <div class="col-md-6">
                                <input id="alt_phone" type="alt_phone" class="form-control{{ $errors->has('alt_phone') ? ' is-invalid' : '' }}" name="alt_phone" value="{{ old('alt_phone') }}" >

                                @if ($errors->has('alt_phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('alt_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="caller_id" class="col-md-4 col-form-label text-md-right">{{ __('Location') }}</label>

                            <div class="col-md-6">
                                
                                <select class="form-control" required="" name="base_station_id" id="base_station_id">

                                    @foreach( $stations as $station)
                                        <option value="{{ $station->id}}">{{$station->name}}</option>
                                    @endforeach()
                                </select>

                                @if ($errors->has('location_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('location_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
