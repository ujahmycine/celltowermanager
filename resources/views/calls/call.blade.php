@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Call') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="connecting col-md-6">
                            <img src="{{asset('images/beacon-tower.gif')}}" id="connecting" style="display: none;">
                        </div>
                        <div class="ringing col-md-6">
                            <img src="{{asset('images/phone-gif-2.gif')}}" id="ringing" height="270" width="250"  style="display: none;">
                            <audio src="" controls="controls" autoplay="true" style="visibility: hidden;" id="audio">
Your browser does not support the audio element.
</audio>
                        </div>
                        <div class="connected" id="connected" style="display: none;">
                            call in progress timer
                            <label id="minutes">00</label>:<label id="seconds">00</label>
                            <p>
                                <label id="Tseconds">00</label></p>
                        </div>
                    </div>
                    
                    <form method="post" >
                        <div class="form-group row">
                            <label for="caller_id" class="col-md-4 col-form-label text-md-right">{{ __('Your Number') }}</label>

                            <div class="col-md-6">
                                
                                <select class="form-control" required="" name="caller_id" id="caller_id">

                                    @foreach($myNumber as $number)
                                        <option value="{{ $number->id}}">{{$number->number}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          <div class="form-group row">
                            <label for="receiver_id" class="col-md-4 col-form-label text-md-right">{{ __('Receiver') }}</label>

                            <div class="col-md-6">
                                
                                <select class="form-control" required="" name="receiver_id" id="receiver_id">

                                    @foreach( $numbers as $number)
                                        <option value="{{ $number->id}}">{{$number->number}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="call">
                                    {{ __('Call') }}
                                </button>
                                
                                    <button type="submit" class="btn btn-primary" id="endcall"  style="display: none;">
                                        End Call
                                    </button>
                                
                                
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="col-md-">
            <div class="card">
                <div class="card-header">Call Log </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead>
                        <tr>
                          <th scope="col">SN</th>
                          <th scope="col">Caller</th>
                          <th scope="col">Receiver</th>
                          <th scope="col">Time(sec)</th>
                          <th scope="col">Charge (#)</th>
                          <th scope="col">Time</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($calls as $key=>$call)
                        <tr>
                          <th scope="row">{{$key+1}}</th>
                          <td>{{$call->callNo->number}}</td>
                          <td>{{$call->receiverNo->number}}</td>
                          <td>{{$call->airtime}}</td>
                          <td>{{$call->charge}}</td>
                          <td>{{$call->created_at->format('Y-m-d D H:i:s')}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


 <script type="text/javascript" src="{{asset('/js/jquery-3.3.1.min.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){

    $("#endcall").click(function(g){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        g.preventDefault();

        var timex = document.getElementById('Tseconds').textContent;
        
        var url = "{{route('end-call')}}";
        var formData = {
                   "_token": token,
                   "id": $('#id').val(),
                   "caller_id": $('#caller_id').val(),
                   "receiver_id": $('#receiver_id').val(),
                   "airtime" : timex
               }
        var type = "post";
        var token = '<?php echo csrf_token(); ?>';

        $.ajax({
            type: type,
            url: url,
            data: formData,
            dataType: 'json',
            success: function (response) {   
                console.log("Call Ended " + response);
                $('#connected').fadeIn(2000);
                

                

            },
            error: function (data) {
                alert("Something went wrong, Try again latter or contact the administrator for more details");
                
                
                console.log('Error:', data);
            }
        });


    });
  
    
    $("#call").click(function(e){
        
        var url = "{{route('calling')}}";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })

        e.preventDefault();

        $('#connecting').fadeIn(200);
        $('#endcall').fadeIn(2000);

        var minutesLabel = document.getElementById("minutes");
        var secondsLabel = document.getElementById("seconds");
        var Tsecondslabel = document.getElementById("Tseconds");
        var totalSeconds = 0;
        

        function setTime() {
          ++totalSeconds;
          secondsLabel.innerHTML = pad(totalSeconds % 60);
          minutesLabel.innerHTML = pad(parseInt(totalSeconds / 60));
          Tsecondslabel.innerHTML = pad(totalSeconds);
        }

        function pad(val) {
          var valString = val + "";
          if (valString.length < 2) {
            return "0" + valString;
          } else {
            return valString;
          }
        }


        var type = "post";
        var token = '<?php echo csrf_token(); ?>';

        var formData = {
                   "_token": token,
                   "id": $('#id').val(),
                   "caller_id":$('#caller_id').val(),
                   "receiver_id": $('#receiver_id').val()
               }
        var audiosrc = "{{asset('audio/ringing.mp3')}}";
            


        setTimeout(function(){
                    
            $.ajax({
            type: type,
            url: url,
            data: formData,
            dataType: 'json',
            success: function (response) {   
                console.log("Call Connected " + response);
                var y = response.toString();
                var x = 'true';
                

                if(x.toLowerCase() === y.toLowerCase())
                {
                    $('#ringing').fadeIn(2000);
                    document.getElementById('audio').src = "{{asset('audio/ringing.mp3')}}";
                    setTimeout(function(){
                        document.getElementById('ringing').style.display='none';
                        document.getElementById('connecting').style.display='none';
                        document.getElementById('audio').src = '';
                        setInterval(setTime, 1000);
                        $('#connected').fadeIn(2000);

                    }, 8000);

                    
                }
                else
                {
                    alert("Our systems are busy");
                    document.getElementById('ringing').style.display='none';
                    document.getElementById('connecting').style.display='none';
                    document.getElementById('audio').src = "{{asset('audio/busy.mp3')}}";
                }
                

            },
            error: function (data) {
                alert("Something went wrong, Try again latter or contact the administrator for more details");
                document.getElementById('audio').src = "{{asset('audio/busy.mp3')}}";
                setTimeout(function(){
                    
                    document.getElementById('audio').src = '';
                    setInterval(setTime, 1000);
                    $('#connected').fadeIn(2000);

                }, 8000);

                console.log('Error:', data);
            }
        });

        }, 8000);
            
    });
     
});
    function showImage()
    {
        // document.getElementById('connecting').style.display='block';
        $('#connecting').fadeIn(5000);

        setTimeout(function(){
            // document.getElementById('ringing').style.display='block';

            $('#ringing').fadeIn(2000);
        }, 10000);
        setTimeout(function(){
            document.getElementById('ringing').style.display='none';
            document.getElementById('connecting').style.display='none';

            $('#connected').fadeIn(2000);

        }, 15000);
    }
   
</script>
@endsection
