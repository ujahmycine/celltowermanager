@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        @if(isset($users))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Reistered Users Report</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead>
                        <tr>
                          <th scope="col">SN</th>
                          <th scope="col">Name</th>
                          <th scope="col">Phone number</th>
                          <th scope="col">Location</th>
                          <th scope="col">Next of Kin</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                          <th scope="row">{{$key+1}}</th>
                          <td>{{$user->first_name}} {{$user->other_name}} {{$user->last_name}}</td>
                          <td>
                          @foreach($user->sims as $sim)
                            {{$sim->number}} ;
                          @endforeach
                          </td>
                          
                          <td>
                        @foreach($user->sims as $sim)
                            <a href="{{route('congestion-details', $sim->baseStation->id)}}">
                                {{$sim->baseStation->name}};
                            </a>
                            
                          @endforeach
                            </td>
                          <td>{{$user->next_of_kin}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif

        @if(isset($calls))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Call Log Report</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead>
                        <tr>
                          <th scope="col">SN</th>
                          <th scope="col">Caller</th>
                          <th scope="col">Receiver</th>
                          <th scope="col">Time(sec)</th>
                          <th scope="col">Charge (#)</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($calls as $key=>$call)
                        <tr>
                          <th scope="row">{{$key+1}}</th>
                          <td>{{$call->callNo->number}}</td>
                          <td>{{$call->receiverNo->number}}</td>
                          <td>{{$call->airtime}}</td>
                          <td>{{$call->charge}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
        @if(isset($stations))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Base Stations Report</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead>
                        <tr>
                          <th scope="col">SN</th>
                          <th scope="col">Name</th>
                          <th scope="col">Location</th>
                          <th scope="col">Capacity</th>
                          <th scope="col">Connections</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($stations as $key=>$station)
                        <tr>
                          <th scope="row">{{$key+1}}</th>
                          <td><a href="{{route('congestion-details', $station->id)}}">
                                {{$station->name}}
                            </a>
                            </td>
                          <td>{{$station->location->name}}</td>
                          <td>{{$station->capacity}}</td>
                          <td>{{$station->connected_lines}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
        @if(isset($sims))
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$base->name}}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" id="example">
                      <thead>
                        <tr>
                          <th scope="col">SN</th>
                          <th scope="col">Phone Number</th>
                          <th scope="col">Base Station</th>
                          <th scope="col">Customer</th>
                          <th scope="col">balance (#)</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($sims as $key=>$sim)
                        <tr>
                          <th scope="row">{{$key+1}}</th>
                          <td>{{$sim->number}}</td>
                          <td>{{$sim->baseStation->name}}</td>
                          <td>{{$sim->user->name()}}</td>
                          <td>{{$sim->balance}}</td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
