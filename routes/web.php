<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/call', 'CallController@initiate')->name('call');
Route::post('/calling', 'CallController@calling')->name('calling');
Route::post('/endcall', 'CallController@endcall')->name('end-call');
Route::get('/report', 'HomeController@report')->name('report');
Route::get('/baseStations', 'HomeController@stationReport')->name('congestion');
Route::get('/baseStation/{id}', 'HomeController@singleStationReport')->name('congestion-details');
Route::get('/log', 'HomeController@log')->name('log');